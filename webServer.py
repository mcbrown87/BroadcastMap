from flask import Flask, request, Response
from flask import render_template

import urllib.request
import os

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

app = Flask(__name__)

@app.route('/mapSerivce', methods=['GET'])
def mapService():
    z = request.args.get('z', None)
    x = request.args.get('x', None)
    y = request.args.get('y', None)

    return Response(urllib.request.urlopen(f'https://api.mapbox.com/styles/v1/binniemedia/cjidg826q19632sp7y3mprvs7/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYmlubmllbWVkaWEiLCJhIjoiY2podXVzZTV4MHJtODN2bjU4dWd1dWRncyJ9.jL5M7hAA6kb0kosBrGOE3Q').read(), mimetype='image/png')

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', server=os.environ.get('SERVER_NAME') if os.environ.get('SERVER_NAME') else "localhost")

http_server = HTTPServer(WSGIContainer(app))
http_server.listen(os.environ.get('PORT'))  # serving on port 
IOLoop.instance().start()
