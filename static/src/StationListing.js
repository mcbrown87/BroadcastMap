class StationListing {
    constructor(filePath) {
        this._filePath = filePath
    }

    async getListings() {
        return new Promise(resolve => {
            $.get(this._filePath, fileContents => {
                let lines = fileContents.split("\n")
                resolve(Enumerable.From(lines).Skip(3).Select(x => x.split(",")[0]).ToArray())
            }, 'text')
        })
    }
}