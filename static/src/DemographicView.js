class DemographicView {
    constructor(demographicDataService) {
        this._demographicDataService = demographicDataService
    }

    async getView(coordinate) {
        let demographicData = await this._demographicDataService.getData(coordinate)
        let tableHtml =
            `<table class="table table-bordered">
            <tr>
                <th>Highschool Graduate Rate</th>
                <td>${(demographicData.educationHighSchoolGraduate * 100).toFixed(2)}%</td>
            </tr>
            <tr>
                <th>Bachelors Degree Rate</th>
                <td>${(demographicData.educationBachelorOrGreater * 100).toFixed(2)}%</td>
            </tr>
            <tr>
                <th>Median Income</th>
                <td>$${demographicData.medianIncome}</td>
            </tr>
        </table>`

        return tableHtml
    }
}