class ServiceContourCurrent {
    constructor(filePath) {
        this._filePath = filePath
    }

    async getData() {
        return new Promise(resolve => {
            $.get(this._filePath, fileContents => {
                let lines = fileContents.split("\n")
                let data = []
                for (var index in lines) {
                    let line = lines[index]
                    let lineData = line.split("|")
                    let stationName = lineData[2]

                    if (!stationName) {
                        continue
                    }

                    let centerCoordinate = [lineData[3].split(",")[0].trim(), lineData[3].split(",")[1].trim()]

                    let coordinates = Enumerable.From(lineData).Skip(4).Where(x => !x.includes("^")).Select(x => [x.split(",")[0], x.split(",")[1]]).ToArray()
                    data.push({
                        stationName: stationName.trim(),
                        coordinates: coordinates,
                        centerCoordinate: centerCoordinate
                    })
                }
                resolve(data)
            }, 'text')
        })
    }
}