class ContourView {
    constructor(map, defaultZoomSetter, serviceContourRepository, addToMap, zoomSetter) {
        this._map = map
        this._defaultZoomSetter = defaultZoomSetter
        this._serviceContourRepository = serviceContourRepository
        this._displayedContours = []
        this._addToMap = addToMap != null ? addToMap : (coords, stationName, map) => {
            return L.polygon(coords, {
                color: "FFFFFF",
                fillOpacity: 0.25,
                weight: 3
            }).bindTooltip(stationName).addTo(map)
        }

        this._zoomSetter = zoomSetter != null ? zoomSetter : (displayedPolylines) => {
            this._map.fitBounds(L.featureGroup(displayedPolylines).getBounds())
        }
    }

    async addContour(stationName) {
        this._map.spin(true)
        let serviceContours = await this._serviceContourRepository.getData()
        this._map.spin(false)
        Enumerable.From(serviceContours).Where(x => x.stationName.includes(stationName)).ToArray().forEach(async x => {

            let polyline = this._addToMap(x.coordinates, x.stationName, this._map)

            this._displayedContours.push({
                stationName: x.stationName,
                polyline: polyline
            })
        })

        await this._updateZoom()
    }

    async removeContour(stationName) {
            Enumerable.From(this._displayedContours).Where(x => x.stationName.includes(stationName.split("-")[0])).Select(x => x.polyline).ToArray().forEach(polylineToRemove => {
            this._map.removeLayer(polylineToRemove)

            let enumerable = Enumerable.From(this._displayedContours)
            let dictionary = enumerable.ToDictionary()
            dictionary.Remove(enumerable.First(x => x.stationName.includes(stationName.split("-")[0])))
            this._displayedContours = dictionary.ToEnumerable().Select(s => s.Key).ToArray()
        })

        await this._updateZoom()
    }

    _updateZoom() {
        let displayedPolylines = Enumerable.From(this._displayedContours).Select(x => x.polyline).ToArray()
        if (displayedPolylines.length == 0) {
            this._defaultZoomSetter()
        } else {
            this._zoomSetter(displayedPolylines)
        }
    }
}
