class ServiceContourAggregator {

    constructor() {
        this._data = []
    }

    async getData() {
        if (this._data.length == 0) {
            for (var i in [...Array(10).keys()]) {
                let blah = await (new ServiceContourCurrent(`static/FM_service_contour_current${parseInt(i)+1}.txt`).getData())
                this._data = this._data.concat(blah)
            }
        }

        return this._data
    }
}