class DemographicService {
    async getData(coordinate) {
        return new Promise(resolve => {
            $.getJSON(`https://www.broadbandmap.gov/broadbandmap/demographic/2014/coordinates?latitude=${coordinate[0]}&longitude=${coordinate[1]}&format=json`, (data) => {
                resolve(data.Results)
            })
        })
    }
}