class BinnieMediaServiceContours {
    constructor(serviceContourCurrent) {
        this._serviceContourCurrent = serviceContourCurrent
        this._data = null
    }

    async getData() {
        if (this._data == null) {
            this._data = await this._serviceContourCurrent.getData()
        }

        return this._data
    }
}