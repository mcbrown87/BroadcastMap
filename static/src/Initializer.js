class Initializer {
    constructor(map, defaultViewSetter, serviceContourRepository, server) {
        this._map = map
        this._defaultViewSetter = defaultViewSetter
        this._serviceContourRepository = serviceContourRepository
        this._server = server
    }

    async initialize() {
        L.tileLayer(
            `${this._server}/mapSerivce?z={z}&x={x}&y={y}`, {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets'
            }).addTo(this._map);
        this._defaultViewSetter()
        this._map.spin(true)
        let selectData = Enumerable.From(await this._serviceContourRepository.getData()).Select("x => {id:x['stationName'], text:x['stationName']}").ToArray()

        $('.js-example-basic-multiple').select2({
            placeholder: 'Select an option',
            data: selectData
        });

        this._map.spin(false)
    }
}
