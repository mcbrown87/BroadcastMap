from py_linq import Enumerable
import os

def main():
    # set up relative paths so other contributors can use this
    script_dir = os.path.dirname(os.path.abspath(__file__))
    fm_service_full = os.path.join(script_dir, 'static', 'FM_service_contour_current_full.txt')
    gmpt_station_listing = os.path.join(script_dir, 'static', 'Copy of GMP-GMPT Station listing.xlsx - Stations.csv')
    fm_service_current = os.path.join(script_dir, 'static', 'FM_service_contour_current.txt')

    with open(fm_service_full) as serviceContourFile, open(gmpt_station_listing) as listingFile:
        listings = Enumerable(listingFile.readlines()).skip(3).select( lambda x: x.split(",")[0].split("-")[0])
        serviceContourLines = Enumerable(serviceContourFile.readlines()).where(lambda x: listings.any(lambda y: y in x))

    with open(fm_service_current, "w") as outFile:
        for line in serviceContourLines:
            outFile.write(line)

def split():
    # set up relative paths so other contributors can use this
    script_dir = os.path.dirname(os.path.abspath(__file__))
    fm_service_current = os.path.join(script_dir, 'static', 'FM_service_contour_current.txt')

    with open(fm_service_current) as file:
        lines = file.readlines()

        for i in range(len(lines)):
            with open(os.path.join(script_dir, 'static', 'FM_service_contour_current{0}.txt'.format((i % 10) + 1)), "a") as outFile:
                outFile.write(lines[i])

if __name__ == "__main__":
    split()