describe("BinnieMediaServiceContours", () => {
    it("should cache it's data", async () => {
        mockServiceContourCurrent = {
            getData: async function() {}
        }
        
        spyOn(mockServiceContourCurrent, 'getData').and.returnValue("blah");

        let sut = new BinnieMediaServiceContours(mockServiceContourCurrent)
        await sut.getData()
        await sut.getData()

        expect(mockServiceContourCurrent.getData.calls.count()).toBe(1);
    })
})