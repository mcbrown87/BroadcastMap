describe("ContourView", () => {

    beforeEach(() => {
        this.mockMap = {
            spin: async function() {},
            removeLayer: function() {}
        }

        this.mockServiceContourRepository = {
            getData: async function() {}
        }

        this.mockDefaultZoomSetter = {
            defaultZoomSetter: function() {}
        }
    })

    it("should be able to add a contour if it exists", async () => {
        spyOn(mockMap, 'spin')
        spyOn(mockServiceContourRepository, 'getData').and.returnValues([{coordinates:[], stationName: "blah"}])

        let defaultZoomSetterCallCount = 0
        let zoomSetterCallCount = 0

        let sut = new ContourView(mockMap, () => { defaultZoomSetterCallCount }, mockServiceContourRepository, () => {return "polyline"}, () => { zoomSetterCallCount++ })
        await sut.addContour("blah")

        expect(mockMap.spin.calls.count()).toBe(2);
        expect(sut._displayedContours.length).toBe(1)
        expect(defaultZoomSetterCallCount).toBe(0)
        expect(zoomSetterCallCount).toBe(1)
    })

    it("shouldn't be able to add a contour if it doesn't exist", async () => {
        spyOn(mockServiceContourRepository, 'getData').and.returnValues([{coordinates:[], stationName: "blah"}])

        let sut = new ContourView(mockMap, () => {}, mockServiceContourRepository, () => {})
        await sut.addContour("I don't exisit")

        expect(sut._displayedContours.length).toBe(0)
    })

    it("should be able to remove a contour", () => {
        spyOn(mockMap, 'removeLayer')
        let defaultZoomSetterCallCount = 0
        let zoomSetterCallCount = 0
        let sut = new ContourView(mockMap, () => { defaultZoomSetterCallCount++ }, mockServiceContourRepository, () => {}, () => { zoomSetterCallCount++ })
        sut._displayedContours = [{stationName: 'blah', polyline : []}]

        sut.removeContour("blah")
        expect(mockMap.removeLayer.calls.count()).toBe(1)
        expect(sut._displayedContours.length).toBe(0)
        expect(defaultZoomSetterCallCount).toBe(1)
        expect(zoomSetterCallCount).toBe(0)
    })
})