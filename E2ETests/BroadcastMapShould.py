import unittest

from selenium import webdriver

class BroadcastMap():
    def __init__(self, webdriver):
        self._webdriver = webdriver
        self._searchInput = lambda : webdriver.find_element_by_css_selector("input")
        self._searchResults = lambda : webdriver.find_elements_by_css_selector("li.select2-results__option")
        self._displayedContours = lambda : webdriver.find_elements_by_css_selector("path")
        self._selectedContours = lambda : webdriver.find_elements_by_css_selector(".select2-selection__choice")

    def enterSearchString(self, searchString):
        self._searchInput().send_keys(searchString)

    @property
    def searchResults(self):
        return self._searchResults()

    @property
    def selectedContours(self):
        return self._selectedContours()

    def removeSelectedContour(self, selectedContour):
        selectedContour.find_element_by_css_selector(".select2-selection__choice__remove").click()

    @property
    def displayedContours(self):
        return self._displayedContours()

    @staticmethod
    def invoke(webdriver, broadcastMapUrl):
        webdriver.get(broadcastMapUrl)
        return BroadcastMap(webdriver)

class BroadcastMapShould(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument('disable-infobars')
        chrome_options.add_argument('start-maximized')
        self.webdriver = webdriver.Chrome(options=chrome_options)
        self.webdriver.implicitly_wait(5)
        self.addCleanup(self.webdriver.quit)
        self.broadcastMap = BroadcastMap.invoke(self.webdriver, 'http://web/')

    def testDisplayCorrectTitle(self):
        self.assertIn('GML FM Coverage Map', self.webdriver.title)

    def testAllowUserToSearchContours(self):
        self.broadcastMap.enterSearchString("fnk")
        self.assertEqual(2, len(self.broadcastMap.searchResults))

    def testDisplaysAllContours(self):
        self.assertEqual(0, len(self.broadcastMap.displayedContours))

        self.broadcastMap.enterSearchString("fnk")
        self.broadcastMap.searchResults[0].click()

        self.broadcastMap.enterSearchString("wfnq")
        self.broadcastMap.searchResults[0].click()

        self.assertEqual(2, len(self.broadcastMap.displayedContours))
        for displayedContour in self.broadcastMap.displayedContours:
            self.assertTrue(displayedContour.is_displayed())

    def testBeAbleToRemoveContours(self):
        self.broadcastMap.enterSearchString("fnk")
        self.broadcastMap.searchResults[0].click()

        self.broadcastMap.enterSearchString("wfnq")
        self.broadcastMap.searchResults[0].click()

        self.assertEqual(2, len(self.broadcastMap.displayedContours))

        while len(self.broadcastMap.selectedContours) > 0:
            self.broadcastMap.removeSelectedContour(self.broadcastMap.selectedContours[0])

        self.assertEqual(0, len(self.broadcastMap.displayedContours))

if __name__ == '__main__':
    unittest.main(verbosity=2)
